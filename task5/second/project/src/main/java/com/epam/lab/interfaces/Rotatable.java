package com.epam.lab.interfaces;

/**
 * This interface contains common behavior for astronomical objects, that can rotate.
 * For educational purpose, star CAN NOT rotate. In the real life, star can rotate.
 */

public interface Rotatable {

    /**
     * This method describes logic of self-rotating astronomical objects.
     */
    void selfRotate();
}
