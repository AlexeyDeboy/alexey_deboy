package com.epam.lab.entities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class describes a model of a star in space.
 * - extends - AstronomicalObject
 * @author – Alexey Deboy
 */

public class Star extends AstronomicalObject {

    /**
     * This field contains information about spectral class of star.
     */
    private char spectralClass;

    /**
     * This is logger for logging some information.
     */
    private static final Logger LOGGER = LogManager.getLogger(Star.class);

    public char getSpectralClass() {
        return spectralClass;
    }

    public void setSpectralClass(char spectralClass) {
        this.spectralClass = spectralClass;
    }

    /**
     * This is public constructor for creating some objects.
     * @param core Core in input
     * @param name String in input
     * @param weight double in input
     * @param radius double in input
     * @param averageTemperature double in input
     * @param firstSpaceSpeed double in input
     * @param spectralClass char in input
     */

    public Star(Core core, String name, double weight, double radius, double averageTemperature, double firstSpaceSpeed, char spectralClass) {
        super(core, name, weight, radius, averageTemperature, firstSpaceSpeed);
        this.spectralClass = spectralClass;
    }

    public boolean isGoodForLife() {
        return false;
    }

    @Override
    public void move() {
       LOGGER.info("Stars move in the vastness of the universe.");
    }

    @Override
    public String toString() {
        return super.toString() + "Spectral class = \"" + spectralClass+"\"\n\n";
    }
}
