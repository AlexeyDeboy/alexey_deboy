package com.epam.lab;

import com.epam.lab.entities.Comet;
import com.epam.lab.entities.Core;
import com.epam.lab.entities.Planet;
import com.epam.lab.entities.Star;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This is Runner class
 * @author – Alexey Deboy
 */


public class Runner {

    /**
     * This is logger for logging information
     */
    private static final Logger LOGGER = LogManager.getLogger(Runner.class);


    /**
     * This is main method that describes behavior of my solar system.
     * @param args String[] in input
     */
    public static void main(String[] args) {

        LOGGER.info("Welcome to my solar system!");

        Planet planet = new Planet(
                new Core(45_695, 3400),
                "4546B",
                495_8495,
                540_405_955.1,
                15,
                405_404,
                10);

        planet.selfRotate();
        planet.move();

        boolean genesis = planet.isGoodForLife();

        StringBuilder strPlanet = new StringBuilder("Planet");
        strPlanet.append(planet);

        if (genesis) {
            strPlanet.append(" is suitable for life!\n");
            LOGGER.info(strPlanet.toString());
        } else {
            strPlanet.append(" is unsuitable for life.\n");
            LOGGER.info(strPlanet.toString());
        }

        Star star = new Star(
                new Core(34_768, 2400),
                "Regul",
                1_000_000,
                740_059_999,
                3000,
                7.91,
                'A');
        star.move();

        StringBuilder strStar = new StringBuilder("Life on Star:");

        genesis = star.isGoodForLife();
        strStar.append(star);
        strStar.append(genesis);
        LOGGER.info(strStar.toString());

        Comet comet = new Comet(
                new Core(1980, 300),
                "D485/A",
                34_534,
                13_000,
                190,
                1.13,
                350_000);

        comet.move();
        comet.selfRotate();

        StringBuilder strComet = new StringBuilder("Life on Comet");

        genesis = comet.isGoodForLife();

        strComet.append(comet);
        strComet.append(genesis);
        LOGGER.info(strComet.toString());
    }

}
