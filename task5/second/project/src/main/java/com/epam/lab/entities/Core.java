package com.epam.lab.entities;

/**
 * This class describes model of a core of astronomical objects. This class shows "has-a" relationship.
 * @author – Alexey Deboy
 */

public class Core {


    /**
     * This field contains information about radius of a core.
     */
    private double radius;

    /**
     * This field contains information about depth of a core.
     */
    private double depth;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }

    /**
     * This is public constructor for creating some objects
     * @param radius double in input
     * @param depth double in input
     */

    public Core(double radius, double depth) {
        this.radius = radius;
        this.depth = depth;
    }

    @Override
    public String toString() {
        return "radius = " + radius + " m\n" +
                " depth = " + depth + " m\n";
    }
}
