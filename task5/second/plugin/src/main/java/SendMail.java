import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

@Mojo(name = "sendMail" , defaultPhase = LifecyclePhase.COMPILE)
public class SendMail extends AbstractMojo {

    @Parameter(property = "login")
    private String to;

    @Parameter(property = "password")
    private String password;

    @Parameter(property = "subject")
    private String subject;

    @Parameter(property = "text")
    private String text;

    @Parameter(property = "email")
    private String from;

    public void execute() throws MojoExecutionException {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com"); 
        props.put("mail.smtp.port", "587"); 
        props.put("mail.smtp.auth", "true"); 
        props.put("mail.smtp.starttls.enable", "true"); 

        
        Authenticator auth = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, "AMP-7mF-qBM-RcM");
            }
        };
        Session session = Session.getInstance(props, auth);

        EmailUtil.sendEmail(session, to,subject, text);
    }
}
