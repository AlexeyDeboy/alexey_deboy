package com.epam.lab.interfaces;

/**
 * This interface contains common behavior for all astronomical object, that can have life.
 */

public interface Genesisable {

    /**
     * This method describes logic of genesis on the astronomical object.
     * @return boolean value. false - if object can not have live, true - if object can have life
     */
    boolean isGoodForLife();
}
