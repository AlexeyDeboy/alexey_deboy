package com.epam.lab.entities;

import com.epam.lab.interfaces.Rotatable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * This class describes a model of a planet in space.
 * - extends - AstronomicalObject
 * - implements – Rotatable
 * @author – Alexey Deboy
 */

public class Planet extends AstronomicalObject implements Rotatable {

    /**
     * This field contains information about rotation period of a planet
     */
    private double rotationPeriod;

    /**
     * This is logger for logging some information
     */
    private static final Logger LOGGER = LogManager.getLogger(Planet.class);

    public double getRotationPeriod() {
        return rotationPeriod;
    }

    public void setRotationPeriod(double rotationPeriod) {
        this.rotationPeriod = rotationPeriod;
    }

    /**
     * This is public constructor for creating some objects
     * @param core Core in input
     * @param name String in input
     * @param weight double in input
     * @param radius double in input
     * @param avgTemp double in input
     * @param firstSpaceSpeed double in input
     * @param rotationPeriod double in input
     */

    public Planet(Core core, String name, double weight, double radius, double avgTemp, double firstSpaceSpeed, double rotationPeriod) {
        super(core,name,weight,radius,avgTemp,firstSpaceSpeed);
        this.rotationPeriod = rotationPeriod;
    }

    @Override
    public void move() {
        LOGGER.info("A planet can move in its orbit.");
    }

    public boolean isGoodForLife() {
        return getRadius() > 666 && getAverageTemperature() == 15;
    }

    public void selfRotate() {
        LOGGER.info("A planet moves around its axis.");
    }

    @Override
    public String toString() {
        return super.toString() + "rotation period = " + rotationPeriod + " sol\n\n";
    }
}
