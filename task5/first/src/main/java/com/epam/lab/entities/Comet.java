package com.epam.lab.entities;

import com.epam.lab.interfaces.Rotatable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class describes a model of a comet in space.
 * - extends - AstronomicalObject
 * - implements – Rotatable
 * @author – Alexey Deboy
 */

public class Comet extends AstronomicalObject implements Rotatable {

    /**
     * This field contains information about length of tail of comet
     */
    private double tailLength;

    /**
     * This is logger for logging some information.
     */
    private static final Logger LOGGER = LogManager.getLogger(Comet.class);

    public double getTailLength() {
        return tailLength;
    }

    public void setTailLength(double tailLength) {
        this.tailLength = tailLength;
    }

    /**
     * This is public constructor for creating create some objects.
     * @param core Core in input
     * @param name String in input
     * @param weight double in input
     * @param radius double in input
     * @param averageTemperature double in input
     * @param firstSpaceSpeed double in input
     * @param tailLength double in input
     */

    public Comet(Core core, String name, double weight, double radius, double averageTemperature, double firstSpaceSpeed, double tailLength) {
        super(core, name, weight, radius, averageTemperature, firstSpaceSpeed);
        this.tailLength = tailLength;
    }


    @Override
    public void move() {
        LOGGER.info("Comet moves by it's orbit.");
    }

    public boolean isGoodForLife() {
        return false;
    }

    public void selfRotate() {
         LOGGER.info("Head of the comet is always rotating by itself");
    }

    @Override
    public String toString() {
        return super.toString() + "Length of the tail = " + tailLength + " m\n\n";
    }
}
