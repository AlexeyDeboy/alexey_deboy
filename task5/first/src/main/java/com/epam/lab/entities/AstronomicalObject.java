package com.epam.lab.entities;

import com.epam.lab.interfaces.Genesisable;

/**
 * This abstract class describes a model of a body in space and contains common fields and methods.
 * - implements – Genesisable.
 * @author – Alexey Deboy
 */

public abstract class AstronomicalObject implements Genesisable {

    /**
     * This field contains information about name of some astronomical object.
     */
    private String name;

    /**
     * This field contains information about weight of some astronomical object.
     */
    private double weight;

    /**
     * This field contains information about radius of some astronomical object.
     */
    private double radius;

    /**
     * This field contains information about average temperature of some astronomical object.
     */
    private double averageTemperature;

    /**
     * This field contains information about first space speed of some astronomical object.
     */
    private double firstSpaceSpeed;

    /**
     * This field contains information about core of some astronomical object.
     */
    private Core core;


    /**
     * This is constructor by default
     */
    public AstronomicalObject(){
        super();
    }

    /**
     * This is the constructor for inheritance classes.
     * @param core Core in input
     * @param name String in input
     * @param weight double in input
     * @param radius double in input
     * @param averageTemperature double in input
     * @param firstSpaceSpeed double in input
     */

    public AstronomicalObject(Core core, String name, double weight, double radius, double averageTemperature, double firstSpaceSpeed) {
        super();
        this.name = name;
        this.core = core;
        this.weight = weight;
        this.radius = radius;
        this.averageTemperature = averageTemperature;
        this.firstSpaceSpeed = firstSpaceSpeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getAverageTemperature() {
        return averageTemperature;
    }

    public void setAverageTemperature(double averageTemperature) {
        this.averageTemperature = averageTemperature;
    }

    public double getFirstSpaceSpeed() {
        return firstSpaceSpeed;
    }

    public void setFirstSpaceSpeed(double firstSpaceSpeed) {
        this.firstSpaceSpeed = firstSpaceSpeed;
    }

    public Core getCore() {
        return core;
    }

    public void setCore(Core core) {
        this.core = core;
    }

    /**
     * This abstract method will describe logic of a movement.
     * For educational purposes, we will only use the logger in the future.
     */
    public abstract void move();


    @Override
    public String toString() {
        return " \"" + name + "\"" + "\n" +
                "weight = " + weight + " kg \n" +
                "radius = " + radius + " m \n" +
                "Average temperature = " + averageTemperature + " С°\n" +
                "First space speed = " + firstSpaceSpeed + " km/h\n\n" +
                "Core: \n " + core + "\n";
    }
}
