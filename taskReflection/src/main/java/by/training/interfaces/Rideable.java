package by.training.interfaces;

import by.training.annotation.LogMethodWork;

public interface Rideable {
    @LogMethodWork
    boolean hasTransmission();
    @LogMethodWork
    void tuningFactor(Long time, Integer coefficient) throws Exception;
}
