package by.training.proxy;

import by.training.models.Motorcycle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MotorcycleInvocationHandler implements InvocationHandler {

    private static final Logger LOGGER = LogManager.getLogger(MotorcycleInvocationHandler.class);

    private Object target;

    public MotorcycleInvocationHandler(Object target){this.target = target;}

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;

            if (target instanceof Motorcycle){
                result = method.invoke(target, args);
                LOGGER.info("result {} {}", result.getClass().getTypeName(), result);
            }

        return result;
    }
}