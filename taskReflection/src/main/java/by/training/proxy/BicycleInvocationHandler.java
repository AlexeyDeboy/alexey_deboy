package by.training.proxy;

import by.training.models.Bicycle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class BicycleInvocationHandler implements InvocationHandler {

    private static final Logger LOGGER = LogManager.getLogger(BicycleInvocationHandler.class);

    private Object target;

    public BicycleInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;

        if (target instanceof Bicycle){
            LOGGER.info("{} uses method {}",Bicycle.class.getSimpleName() , method.getName());
            result = method.invoke(target, args);
        }
        return result;
    }
}