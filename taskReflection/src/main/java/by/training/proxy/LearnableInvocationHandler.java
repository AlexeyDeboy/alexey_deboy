package by.training.proxy;

import by.training.annotation.LogMethodWork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class LearnableInvocationHandler implements InvocationHandler {

    private static final Logger LOGGER = LogManager.getLogger(LearnableInvocationHandler.class);

    private Object target;

    public LearnableInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = null;
        if (method.isAnnotationPresent(LogMethodWork.class)) {
            long time = System.nanoTime();
            result = method.invoke(target, args);
            time = System.nanoTime() - time;
            LOGGER.info("Executing {} finished in {} ns", allMethodInf(method, args, result), time);
        }
        return result;
    }

    private String allMethodInf(Method method, Object[] args, Object result){
        final StringBuilder string = new StringBuilder();

        string.append(Modifier.toString(method.getModifiers())).append(" ")
              .append(method.getReturnType().getSimpleName()) .append(" ")
              .append(method.getName()).append(" ")
              .append("(");

        if (args != null){
            final Class[] types = method.getParameterTypes();
            int length = args.length - 1;
            for (int i = 0; i < length; i++) {
                string.append(types[i].getSimpleName())
                      .append("=")
                      .append(args[i])
                      .append(",");
            }
            string.append(types[length].getSimpleName()) .append("=") .append(args[length]) .append(";");
        }
        string.append("resutl") .append("=") .append(result);
        string.append(")");

        return string.toString();
    }
}