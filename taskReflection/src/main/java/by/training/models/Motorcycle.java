package by.training.models;

import by.training.interfaces.Rideable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Motorcycle implements Rideable {
    private String model;
    private String nameOfDriver;
    private int speed;
    private int capacity;
    private int horsePower;


    @Override
    public void tuningFactor(Long time, Integer coefficient) throws InterruptedException { Thread.sleep(time  * coefficient * coefficient); }

    @Override
    public boolean hasTransmission(){ return true; }
}
