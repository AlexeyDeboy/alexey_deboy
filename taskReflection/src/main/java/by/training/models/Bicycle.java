package by.training.models;

import by.training.interfaces.Rideable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bicycle implements Rideable {
    private String model;
    private String nameOfDriver;
    private int speed;
    private int horsePower;

    @Override
    public void tuningFactor(Long time, Integer coefficient) throws InterruptedException {
        Thread.sleep(time * coefficient);
    }

    @Override
    public boolean hasTransmission() { return false; }
}
