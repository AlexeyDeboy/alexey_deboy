package by.training.proxy;

import by.training.interfaces.Rideable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Proxy;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RideableInvocationHandlerTest {

    @Mock
    private Rideable rideable;

    @InjectMocks LearnableInvocationHandler handler;

    private Rideable proxyRideable;

    @Before
    public void setUp() {
        proxyRideable = (Rideable) Proxy.newProxyInstance(Rideable.class.getClassLoader(),
                            new Class[]{Rideable.class}, handler);
    }

    @Test
    public void hasTransmissionTest() {
        when(rideable.hasTransmission()).thenReturn(true);
        proxyRideable.hasTransmission();
        verify(rideable, times(1)).hasTransmission();
    }

    @Test(expected = Exception.class)
    public void tuningFactorTest() throws Exception {
        doNothing().when(rideable).tuningFactor(anyLong(), anyInt());
        proxyRideable.tuningFactor(100L, 500);
        verify(rideable).tuningFactor(anyLong(), anyInt());

        doThrow(Exception.class).when(rideable).tuningFactor(-10L, 2);
        proxyRideable.tuningFactor(-10L, 2);
    }
}