package by.training.proxy;

import by.training.interfaces.Rideable;
import by.training.models.Motorcycle;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Proxy;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MotorcycleInvocationHandlerTest {

    @Spy private Motorcycle motorcycle;

    @InjectMocks
    MotorcycleInvocationHandler handler;

    private Rideable proxyRideable;

    @Before
    public void setUp() {
        proxyRideable = (Rideable) Proxy.newProxyInstance(Rideable.class.getClassLoader(),
                new Class[]{Rideable.class}, handler);
    }

    @Test
    public void hasTransmissionsTest() {
        when(motorcycle.hasTransmission()).thenReturn(true);
        proxyRideable.hasTransmission();
        verify(motorcycle, times(1)).hasTransmission();
    }

    @Test(expected = Exception.class)
    public void tuningFactorTest() throws Exception {
        doNothing().when(motorcycle).tuningFactor(anyLong(), anyInt());
        proxyRideable.tuningFactor(100L, 500);
        verify(motorcycle).tuningFactor(anyLong(), anyInt());

        doThrow(Exception.class).when(motorcycle).tuningFactor(-10L, 2);
        proxyRideable.tuningFactor(-10L, 2);
    }
}
