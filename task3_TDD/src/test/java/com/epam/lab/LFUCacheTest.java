package com.epam.lab;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LFUCacheTest {

    private LFUCache cache;

    @BeforeEach
    void setUp() {
        cache = new LFUCache(2);
    }

    @Test
    void testCacheStartsEmpty() {
        assertEquals(cache.get(1), -1);
    }

    @Test
    void testSetBelowCapacity() {
        cache.set(1, 1);
        assertEquals((int)cache.get(1), 1);
        assertEquals(cache.get(2), -1);
        cache.set(2, 4);
        assertEquals((int)cache.get(1), 1);
        assertEquals((int)cache.get(2), 4);
    }

    @Test
    void testCapacityReachedOldestRemoved() {
        cache.set(1, 1);
        cache.set(2, 4);
        cache.set(3, 9);
        assertEquals(cache.get(1), -1);
        assertEquals((int)cache.get(2), 4);
        assertEquals((int)cache.get(3), 9);
    }

    @Test
    void testGetRenewsEntry() {
        cache.set(1, 1);
        cache.set(2, 2);
        assertEquals((int)cache.get(1), 1);
        cache.set(3, 9);
        assertEquals((int)cache.get(1), 1);
        assertEquals(cache.get(2), -1);
        assertEquals((int)cache.get(3), 9);
    }
}