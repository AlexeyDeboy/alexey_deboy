package com.epam.lab;

/**
 * This interface represents Cache
 * @param <K> in input
 * @param <V> in input
 * @author - Alexey Deboy
 */

public interface Cache<K, V> {

    V get(K key);

    void set(K key, V value);

    void remove(K key);

    int size();

}
