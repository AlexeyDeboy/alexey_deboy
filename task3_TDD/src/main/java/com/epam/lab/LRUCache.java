package com.epam.lab;

import java.util.HashMap;
import java.util.Map;

/**
 * LRUCache is a class that shows LRU caching algorithm
 * -implements Cache
 * @param <K> in input
 * @param <V> in input
 * @author - Alexey Deboy
 */

public class LRUCache<K, V> implements Cache<K, V> {

    /**
     * This field represents capacity of cache
     */
    private int capacity;

    /**
     * This filed represents head of cache and tail
     */
    private Node<K, V> head, tail;

    /**
     * This is our map for cache
     */
    private Map<K, Node<K, V>> map;

    private static final int DEFAULT = 10;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
    }

    public LRUCache() {
        this(DEFAULT);
    }

    /**
     * This method describes logic of getting a value from cache
     * @param key K in input
     * @return V
     */

    public V get(K key) {
        V result = null;
        Node<K, V> node = this.map.get(key);
        if (node != null) {
            result = node.value;
            remove(node);
            addAsHead(node);
        }
        return result;
    }

    /**
     * This method describes logic of setting something into cache
     * @param key K in input
     * @param value V in input
     */

    public void set(K key, V value) {
        Node<K, V> node = this.map.get(key);
        if (node == null) {
            Node<K, V> temp = new Node<K, V>(key, value);
            if (this.map.size() < this.capacity) {
                addAsHead(temp);
            } else {
                this.map.remove(this.tail.key);
                remove(this.tail);
                addAsHead(temp);
            }
            this.map.put(key, temp);
        } else {
            node.value = value;
            remove(node);
            addAsHead(node);
        }
    }

    /**
     * This method describes logic of removing node from cache
     * @param node Node<K,V> in input
     */
    private void remove(Node<K, V> node) {
        if (node.pre == null) {
            this.head = node.next;
        } else {
            node.pre.next = node.next;
        }

        if (node.next == null) {
            this.tail = node.pre;
        } else {
            node.next.pre = node.pre;
        }
    }

    /**
     * This method describes logic of adding node as a head
     * @param node Node<K,V> in input
     */
    private void addAsHead(Node<K, V> node) {
        if (this.head == null) {
            this.head = node;
            this.tail = node;
        } else {
            this.head.pre = node;
            node.next = this.head;
            this.head = node;
        }
    }

    /**
     * This method describes logic of removing from cache
     * @param key K in input
     */

    public void remove(K key) {
        Node<K, V> node = this.map.get(key);
        if (node != null) {
            this.remove(node);
        }
    }

    /**
     * This inner class represents a Node
     * @param <S> is a parameter for Node (key)
     * @param <T> is a parameter for Node (value)
     */

    private static class Node<S, T> {
        S key;
        T value;
        Node<S, T> pre;
        Node<S, T> next;

        Node(S key, T value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * This method describes logic of getting a size of map
     * @return int
     */

    public int size() {
        return this.map.size();
    }
}