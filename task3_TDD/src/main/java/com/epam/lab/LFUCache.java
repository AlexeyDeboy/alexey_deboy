package com.epam.lab;

import java.util.*;

/**
 * This class represents LFU caching algorithm
 * @author - Alexey Deboy
 */

public class LFUCache {

    /**
     * This field contains values
     */
    private Map<Integer, Node> values = new HashMap<>();

    /**
     * This field contains counts
     */
    private Map<Integer, Integer> counts = new HashMap<>();

    /**
     * This field represents TreeMap of frequencies
     */
    private TreeMap<Integer, DoubleLinkedList> frequencies = new TreeMap<>();

    /**
     * This field contains max capacity of cache
     */
    private final int MAX_CAPACITY;

    public LFUCache(int capacity) {
        MAX_CAPACITY = capacity;
    }

    /**
     * This method describes logic of getting something from cache
     * @param key int in input
     * @return int
     */
    public int get(int key) {

        if (!values.containsKey(key)) {
            return -1;
        }

        Node node = values.get(key);

        int frequency = counts.get(key);
        frequencies.get(frequency).remove(node);
        removeIfListEmpty(frequency);
        frequencies.computeIfAbsent(frequency + 1, k -> new DoubleLinkedList()).add(node);

        counts.put(key, frequency + 1);
        return values.get(key).value;
    }

    /**
     * This method describes logic of setting something into cache
     * @param key int in input
     * @param value int in input
     */

    public void set(int key, int value) {
        if (!values.containsKey(key)) {

            Node node = new Node(key, value);

            if (values.size() == MAX_CAPACITY) {

                int lowestCount = frequencies.firstKey();
                Node nodeTodelete = frequencies.get(lowestCount).head();
                frequencies.get(lowestCount).remove(nodeTodelete);

                int keyToDelete = nodeTodelete.key();
                removeIfListEmpty(lowestCount);
                values.remove(keyToDelete);
                counts.remove(keyToDelete);
            }

            values.put(key, node);
            counts.put(key, 1);
            frequencies.computeIfAbsent(1, k -> new DoubleLinkedList()).add(node);
        }
    }

    /**
     * This method describes logic of removing something from cache if list empty
     * @param frequency int in input
     */
    private void removeIfListEmpty(int frequency) {
        if (frequencies.get(frequency).size() == 0) {
            frequencies.remove(frequency);
        }
    }

    /**
     * This nested class represents node
     */

    private class Node {
        private int key;
        private int value;
        private Node next;
        private Node prev;

        Node(int key, int value) {
            this.key = key;
            this.value = value;
        }

        int key() {
            return key;
        }

        public int value() {
            return value;
        }
    }

    /**
     * This nested class represents double linked list
     */

    private class DoubleLinkedList {
        private int n;
        private Node head;
        private Node tail;

        void add(Node node) {
            if (head == null) {
                head = node;
            } else {
                tail.next = node;
                node.prev = tail;
            }
            tail = node;
            n++;
        }

        void remove(Node node) {

            if (node.next == null) tail = node.prev;
            else node.next.prev = node.prev;

            if (head.key == node.key) head = node.next;
            else node.prev.next = node.next;

            n--;
        }

        Node head() {
            return head;
        }

        int size() {
            return n;
        }
    }
}