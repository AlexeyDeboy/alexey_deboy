/**
 * That's runner class
 */
public class Runner {

    /**
     * Main method
     * @param args String[] in input
     */
    public static void main(String[] args) {
        final String path = args.length == 0 ? "D:\\Games" : args[0];
        final int percentage = 1;
        BaseCase.run(path);
        ForkJoinTotalSizeFileTreeWalker.run(path, percentage);
        ForkJoinFileTreeWalker.run(path, percentage);
        FileCounter.run(path, percentage);
    }
}