import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

/**
 * This class represents Base Case
 */

public class BaseCase {

    /**
     * That's logger.
     */
    private static final Logger logger = LogManager.getLogger(BaseCase.class);

    /**
     * Number of files.
     */
    private static int files;

    /**
     * This field contains directories.
     */
    private static int directories;

    /**
     * That's total size.
     */
    private static long totalSize;

    /**
     * Walking by tree.
     * @param folder File in input
     */
    public static void treeWalk(final File folder) {
        try {
            for (final File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                    directories++;
                    String[] names = fileEntry.list();
                    if (names != null) {
                        treeWalk(fileEntry);
                    }
                } else {
                    files++;
                    totalSize += fileEntry.length();
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Running our thread...
     * @param path String in input
     */
    public static void run(String path) {
        final File folder = new File(path);
        long startTime = System.currentTimeMillis();
        treeWalk(folder);
        long endTime = System.currentTimeMillis();
        logger.info("Folder count: " + directories);
        logger.info("File count: " + files);
        logger.info("Total size: " + totalSize);
        logger.info("Execution time: " + (endTime - startTime));
    }

}
