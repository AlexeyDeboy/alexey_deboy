import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.atomic.AtomicLong;

/**
 * FileCounter is a class that represents file counter.
 */

public class FileCounter extends RecursiveAction {
    /**
     * That's logger.
     */
    private static final Logger logger = LogManager.getLogger(FileCounter.class);

    /**
     * File counter.
     */
    private static final AtomicLong FILE_COUNTER = new AtomicLong();

    /**
     * Folder counter.
     */
    private static final AtomicLong FOLDER_COUNTER = new AtomicLong();

    /**
     * Total size.
     */
    private static final AtomicLong TOTAL_SIZE = new AtomicLong();

    /**
     * Fork counter.
     */
    private static final AtomicLong FORK_COUNTER = new AtomicLong();

    /**
     * That's threshold.
     */
    private static int threshold;

    /**
     * That's fileList;
     */
    private final List<File> fileList;

    /**
     * That's public counter.
     * @param fileList List<File> in input
     * @param percentThreshold int in input
     */
    FileCounter(List<File> fileList, int percentThreshold) {
        this(fileList);
        threshold = BigDecimal.valueOf(fileList.size() / 100.0 * percentThreshold)
                .setScale(0, RoundingMode.UP)
                .toBigInteger()
                .intValue();
    }

    /**
     * Public counter.
     * @param fileList List<File> in input
     */
    private FileCounter(List<File> fileList) {
        this.fileList = fileList;
    }

    /**
     * Computing...
     */
    @Override
    protected void compute() {
        if (fileList.size() <= threshold) {
            for (File f : fileList) {
                if (f.isDirectory()) {
                    FOLDER_COUNTER.incrementAndGet();
                } else {
                    FILE_COUNTER.incrementAndGet();
                    TOTAL_SIZE.addAndGet(f.length());
                }
            }
        } else {
            FORK_COUNTER.incrementAndGet();

            int halfSize = (fileList.size() + 1) / 2;
            FileCounter f1 = new FileCounter(fileList.subList(0, halfSize));
            FileCounter f2 = new FileCounter(fileList.subList(halfSize, fileList.size()));

            ForkJoinTask.invokeAll(f1, f2);
        }
    }

    /**
     * Running our thread...
     * @param path String in input
     * @param percentage int in input
     */
    public static void run(String path, int percentage) {
        FlattenFileTree flattenTask = new FlattenFileTree(new File("D:\\Poker"));
        long flattenStartTime = System.currentTimeMillis();
        List<File> fileList = ForkJoinPool.commonPool().invoke(flattenTask);
        long flattenEndTime = System.currentTimeMillis();

        FileCounter counterTask = new FileCounter(fileList, percentage);
        long startTime = System.currentTimeMillis();
        ForkJoinPool.commonPool().invoke(counterTask);
        long endTime = System.currentTimeMillis();

        logger.info("Flatten time: " + (flattenEndTime - flattenStartTime));
        logger.info("Fork count: " + FORK_COUNTER.get());
        logger.info("Execution time: " + (endTime - startTime));
        logger.info("Total time: " + ((endTime - startTime) + (flattenEndTime - flattenStartTime)));

    }
}