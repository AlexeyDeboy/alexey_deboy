import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This class represents fork join tree walker
 */

public class ForkJoinTotalSizeFileTreeWalker extends RecursiveAction {
    private static final Logger logger = LogManager.getLogger(ForkJoinTotalSizeFileTreeWalker.class);
    private int threshold;
    private static int percent;
    private final static AtomicLong FILE_COUNTER = new AtomicLong();
    private final static AtomicLong FOLDER_COUNTER = new AtomicLong();
    private final static AtomicLong TOTAL_SIZE = new AtomicLong();
    private final static AtomicLong FORK_COUNTER = new AtomicLong();
    private final File[] fileArray;
    private final static ForkJoinPool pool = new ForkJoinPool();

    /**
     * Public constructor
     * @param fileArray File[] in input
     * @param percentThreshold int in input
     */

    public ForkJoinTotalSizeFileTreeWalker(File[] fileArray, int percentThreshold) {
        this.fileArray = fileArray;
        percent = percentThreshold;
        threshold = BigDecimal.valueOf(428388 / 100.0 * percent)
                .setScale(0, RoundingMode.UP)
                .toBigInteger()
                .intValue();
    }

    /**
     * Public constructor
     * @param threshold int in input
     * @param fileArray File[] in input
     */
    private ForkJoinTotalSizeFileTreeWalker(int threshold, File[] fileArray) {
        this.fileArray = fileArray;
        this.threshold = threshold;
    }

    /**
     * Computing....
     */
    @Override
    protected void compute() {
        int n = fileArray.length;
        if (n <= threshold) {
            treeWalk(fileArray);
        } else {
            File[] a = new File[n >> 1];
            File[] b = new File[n - a.length];
            FORK_COUNTER.incrementAndGet();
            System.arraycopy(fileArray, 0, a, 0, a.length);
            System.arraycopy(fileArray, a.length, b, 0, b.length);

            ForkJoinTotalSizeFileTreeWalker f1 = new ForkJoinTotalSizeFileTreeWalker(threshold, a);
            ForkJoinTotalSizeFileTreeWalker f2 = new ForkJoinTotalSizeFileTreeWalker(threshold, b);
            ForkJoinTask.invokeAll(f1, f2);
        }
    }

    /**
     * Walking by tree...
     * @param files File[] in input
     */
    private static void treeWalk(File[] files) {
        for (final File fileEntry : files) {
            if (fileEntry.isDirectory()) {
                FOLDER_COUNTER.incrementAndGet();
                final File[] temp = fileEntry.listFiles();
                if (temp != null && temp.length != 0) {
                    final ForkJoinTotalSizeFileTreeWalker task = new ForkJoinTotalSizeFileTreeWalker(temp, percent);
                    pool.invoke(task);
                }
            } else {
                FILE_COUNTER.incrementAndGet();
                TOTAL_SIZE.addAndGet(fileEntry.length());
            }
        }
    }

    /**
     * Running our thread...
     * @param path String in input
     * @param percentage int in input
     */
    public static void run(String path, int percentage) {
        ForkJoinTotalSizeFileTreeWalker task = new ForkJoinTotalSizeFileTreeWalker(new File(path).listFiles(), percentage);

        long startTime = System.currentTimeMillis();
        pool.invoke(task);
        long endTime = System.currentTimeMillis();

        logger.info("Fork count: " + FORK_COUNTER.get());
        logger.info("Execution time: " + (endTime - startTime));
    }
}
