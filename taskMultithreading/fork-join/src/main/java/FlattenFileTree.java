import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

/**
 * This class represents Flatten File Tree
 */

public class FlattenFileTree extends RecursiveTask<List<File>> {
    /**
     * That's root.
     */
    private final File root;

    /**
     * Public constructor.
     * @param root File in input
     */
    public FlattenFileTree(File root) {
        this.root = root;
    }

    /**
     * Computing...
     * @return List<File> value
     */
    @Override
    protected List<File> compute() {
        final List<RecursiveTask<List<File>>> taskList = new LinkedList<>();
        final List<File> fileList = new ArrayList<>();
        final File[] files = root.listFiles();

        if (files != null) {
            fileList.addAll(Arrays.asList(files));
            for (final File fileEntry : files) {
                if (fileEntry.isDirectory()) {
                    String[] subFolderNames = fileEntry.list();
                    if (subFolderNames != null && subFolderNames.length != 0) {
                        final RecursiveTask<List<File>> subTask = new FlattenFileTree(fileEntry);
                        subTask.fork();
                        taskList.add(subTask);
                    }
                }
            }

            for (final RecursiveTask<List<File>> t : taskList) {
                fileList.addAll(t.join());
            }

        }
        return fileList;
    }
}