package epam.javalab;

import epam.javalab.classes.*;
import epam.javalab.interfaces.Station;
import epam.javalab.interfaces.Tourist;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Phaser;

public class Bus implements Runnable {
    private static final Logger logger = LogManager.getLogger(Bus.class);
    private final List<Station> roadMap = new LinkedList<>(List.of(new Prague(), new Paris(), new Rome(), new Shore()));
    private final Phaser schedule = new Phaser();
    private List<Tourist> passengers = new ArrayList<>();

    private void board() {
        logger.info("Boarding.");
        for (int i = 0; i < 10; i++) {
            Tourist tourist = new TouristImpl(schedule);
            tourist.setInTravel(true);
            passengers.add(tourist);
        }
    }

    private void deboard() {
        logger.info("Deboarding.");
        for (Tourist t : passengers) {
            t.setInTravel(false);
        }
        passengers = new ArrayList<>();
    }

    @Override
    public void run() {
        schedule.register();
        for (int i = 0; i < 2; i++) {
            board();
            for (Station s : roadMap) {
                logger.info("Arriving to " + s);
                for (Tourist t : passengers) {
                    t.setStation(s);
                    new Thread(t).start();
                }
                schedule.arriveAndAwaitAdvance();
            }
            deboard();
        }
        schedule.arriveAndDeregister();
    }

    public static void main(String[] args)  {
        new Thread(new Bus()).start();
    }
}
