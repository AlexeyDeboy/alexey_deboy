package epam.javalab.classes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class represents cloud.
 */
public class Cloud {
    /**
     * That's logger.
     */
    private static final Logger logger = LogManager.getLogger(Cloud.class);

    /**
     * This field contains number of photos.
     */
    private static final AtomicInteger PHOTOS = new AtomicInteger();

    /**
     * This field contains barrier
     */
    private static final CyclicBarrier BARRIER = new CyclicBarrier(10, new Show());

    /**
     * Uploading photos...
     * @param photos int in input
     */
    public void upload(int photos) {
        try {
            PHOTOS.addAndGet(photos);
            BARRIER.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }


    /**
     * Nested class - entity, that show total photos/
     */
    private static class Show implements Runnable {

        /**
         * Running our thread...
         */
        @Override
        public void run() {
            logger.info("Total photos: " + PHOTOS.get());
        }
    }
}
