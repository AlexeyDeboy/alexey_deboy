package epam.javalab.classes;

import epam.javalab.interfaces.Station;
import epam.javalab.interfaces.Tourist;

/**
 * This class represents abstract station...
 */

public abstract class AbstractStation implements Station {

    /**
     * Visiting place...
     * @param tourist Tourist in input
     */
    @Override
    public final void visit(Tourist tourist) {
        tourist.takePhotos();
        performActivity(tourist);
    }

    /**
     * Performing activity....
     * @param tourist Tourist in input
     */
    protected abstract void performActivity(Tourist tourist);

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
