package epam.javalab.classes;

import epam.javalab.interfaces.Tourist;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * This class represents shore.
 */
public class Shore extends AbstractStation {
    /**
     * That's cloud.
     */
    private static final Cloud cloud = new Cloud();

    /**
     * This field contains barrier.
     */
    private static final CyclicBarrier barrier = new CyclicBarrier(10);

    /**
     * Performing activity...
     * @param tourist Tourist in input
     */
    @Override
    protected void performActivity(Tourist tourist) {
        try {
            tourist.relax();
            barrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
        tourist.uploadPhotos(cloud);
    }
}
