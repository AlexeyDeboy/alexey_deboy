package epam.javalab.classes;

import epam.javalab.interfaces.Tourist;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class represents Prague.
 */
public class Prague extends AbstractStation {

    /**
     * This field contains Lock.
     */
    private static final Lock LOCK = new ReentrantLock();

    /**
     * Performing activity.....
     * @param tourist Tourist in input
     */
    @Override
    protected void performActivity(Tourist tourist) {
        LOCK.lock();
        try {
            tourist.eat();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            LOCK.unlock();
        }
    }
}
