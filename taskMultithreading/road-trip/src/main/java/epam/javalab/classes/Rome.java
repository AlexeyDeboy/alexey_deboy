package epam.javalab.classes;

import epam.javalab.interfaces.Tourist;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This class represents Rome without quarantine :(
 */

public class Rome extends AbstractStation {
    /**
     * That's counter.
     */
    static final AtomicInteger counter = new AtomicInteger();

    /**
     * First barrier.
     */
    static final CyclicBarrier barrier1 = new CyclicBarrier(5);

    /**
     * Second barrier.
     */
    static final CyclicBarrier barrier2 = new CyclicBarrier(5);


    /**
     * Performing activity...
     * @param tourist Tourist in input
     */
    @Override
    protected void performActivity(Tourist tourist) {
        try {
            if (counter.getAndIncrement() % 2 == 0) {
                barrier1.await();
            } else {
                barrier2.await();
            }
            tourist.eat();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
