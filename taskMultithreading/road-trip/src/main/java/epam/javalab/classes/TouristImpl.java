package epam.javalab.classes;

import epam.javalab.interfaces.Station;
import epam.javalab.interfaces.Tourist;

import java.util.concurrent.Phaser;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This class represents a tourist!
 */
public class TouristImpl implements Tourist {

    /**
     * This field contains station.
     */
    Station station;

    /**
     * This final field contains schedule.
     */
    final Phaser schedule;

    /**
     * This field contains number of photos.
     */
    private int photos;

    /**
     * This field contains time of eating.
     */
    private final int eatingTime;

    /**
     * This field contains relaxing time.
     */
    private final int relaxingTime;

    /**
     * Is tourist in travel?
     */
    private boolean isInTravel;

    /**
     * Public constructor
     * @param schedule Phaser in input
     */
    public TouristImpl(Phaser schedule) {
        this.schedule = schedule;
        this.photos = 0;
        this.relaxingTime = ThreadLocalRandom.current().nextInt(1000, 5000);
        this.eatingTime = ThreadLocalRandom.current().nextInt(2000, 7000);
    }

    @Override
    public void takePhotos() {
        photos += ThreadLocalRandom.current().nextInt(1, 5);
    }

    @Override
    public void eat() throws InterruptedException {
        Thread.sleep(eatingTime);
    }

    @Override
    public void relax() throws InterruptedException {
        Thread.sleep(relaxingTime);
    }

    @Override
    public void uploadPhotos(Cloud cloud) {
        cloud.upload(photos);
    }

    public void setInTravel(boolean inTravel) {
        this.isInTravel = inTravel;
    }

    @Override
    public void setStation(Station station) {
        this.station = station;
    }

    /**
     * Running pur thread...
     */
    @Override
    public void run() {
        schedule.register();
        station.visit(this);
        schedule.arriveAndDeregister();
    }
}
