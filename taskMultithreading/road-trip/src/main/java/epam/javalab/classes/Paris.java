package epam.javalab.classes;

import epam.javalab.interfaces.Tourist;

import java.util.concurrent.Semaphore;

/**
 * This class represents Paris.
 */
public class Paris extends AbstractStation {
    /**
     * This field contains semaphore.
     */
    static final Semaphore semaphore = new Semaphore(5);

    /**
     * Performing activity...
     * @param tourist Tourist in input
     */
    @Override
    protected void performActivity(Tourist tourist) {
        try {
            semaphore.acquire();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
        }
    }
}
