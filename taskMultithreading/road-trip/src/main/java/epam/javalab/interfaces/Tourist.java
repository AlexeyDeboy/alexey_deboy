package epam.javalab.interfaces;

import epam.javalab.classes.Cloud;

/**
 * This class represents tourist.
 */

public interface Tourist extends Runnable {
    /**
     * Taking photos...
     */
    void takePhotos();

    /**
     * Eating...
     * @throws InterruptedException exception
     */
    void eat() throws InterruptedException;

    /**
     * Relaxing :)
     * @throws InterruptedException exception
     */
    void relax() throws InterruptedException;

    /**
     * Upload photos...
     * @param cloud Cloud in input
     */
    void uploadPhotos(Cloud cloud);

    /**
     * Setting in travel...
     * @param isInTravel boolean in input
     */
    void setInTravel(boolean isInTravel);

    /**
     * Setting station...
     * @param station Station in input.
     */
    void setStation(Station station);
}
