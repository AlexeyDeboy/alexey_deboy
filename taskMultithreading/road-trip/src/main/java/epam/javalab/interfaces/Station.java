package epam.javalab.interfaces;

/**
 * This interface represents station
 */
public interface Station {
    /**
     * Visiting places
     * @param tourist Tourist in input
     */
    void visit(Tourist tourist);
}
