package epam.javalab.classes;

/**
 * This class represents low effort bounded blocking queue.
 * @param <T>
 */
public class LowEffortBoundedBlockingQueue<T> extends LowEffortBlockingQueue<T> {

    /**
     * That's our capacity
     */
    private final int capacity;

    /**
     * Public constructor
     * @param capacity
     */
    public LowEffortBoundedBlockingQueue(int capacity) {
        super();
        this.capacity = capacity;
    }


    @Override
    public synchronized boolean enqueue(T item) {
        if (getSize() < capacity) {
            return super.enqueue(item);
        }
        return false;
    }
}
