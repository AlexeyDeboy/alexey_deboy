package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * That class represents Mafia
 */

public class Mafia implements Runnable {
    /**
     * That's our logger
     */
    private static final Logger logger = LogManager.getLogger(Mafia.class);

    /**
     * That's queue that represents channel to exchangers
     */
    private final LowEffortQueue<Double> channelToExchangers;

    /**
     * This field contains mafia's ledger
     */
    private double mafiaLedger;

    /**
     * Public constructor
     * @param channelToExchangers LowEffortQueue<Double> in input
     */
    public Mafia(LowEffortQueue<Double> channelToExchangers) {
        this.channelToExchangers = channelToExchangers;
        this.mafiaLedger = 0;
    }

    /**
     * Running out thread...
     */
    @Override
    public void run() {
        while (!channelToExchangers.isEmpty() || !channelToExchangers.isAbandoned()) {
            double moreMoney = channelToExchangers.dequeue();
            if (moreMoney != 0) {
                mafiaLedger = mafiaLedger + moreMoney;
            }
        }
        logger.info("Total mafia ledger: " + mafiaLedger);
    }
}
