package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;
import epam.javalab.interfaces.Tourist;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static epam.javalab.classes.Util.rounding;

/**
 * This class represents exchange on the street..
 */

public class BackstreetExchange extends AbstractExchange {

    /**
     * That's our logger
     */
    private static final Logger logger = LogManager.getLogger(BackstreetExchange.class);

    /**
     * This field contains backstreet rate
     */
    private static final double BACKSTREET_RATE = 0.95;

    /**
     * That's service time for exchange
     */
    private static final int BACKSTREET_SERVICE_TIME = 2000;


    /**
     * This is public constructor
     * @param channelToMafia LowEffortQueue<Double> in input
     * @param exchangeQueue LowEffortQueue<Tourist> in input
     */
    public BackstreetExchange(LowEffortQueue<Double> channelToMafia, LowEffortQueue<Tourist> exchangeQueue) {
        super(channelToMafia, exchangeQueue);
    }

    /**
     * Sleeping...
     * @throws InterruptedException exception
     */
    @Override
    protected void sleep() throws InterruptedException {
        Thread.sleep(BACKSTREET_SERVICE_TIME);
    }

    /**
     * Getting our logger
     * @return Logger
     */
    @Override
    protected Logger getLogger() {
        return logger;
    }

    /**
     * Setting exchange rate
     * @param rate double in input
     */
    @Override
    public void setExchangeRate(double rate) {
        double actualRate = rounding(BACKSTREET_RATE * rate);
        super.setExchangeRate(actualRate);
    }
}
