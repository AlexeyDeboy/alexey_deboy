package epam.javalab.classes;

import epam.javalab.interfaces.Exchange;
import java.util.concurrent.TimeUnit;
import static epam.javalab.classes.Util.rounding;
/**
 * This class represents exchanging rate generator
 */

public class ExchangeRateGenerator implements Runnable {

    /**
     * That's legal exchange
     */
    private final Exchange legal;

    /**
     * That's illegal exchange
     */
    private final Exchange backstreet;

    /**
     * That's our public constructor
     * @param legal Exchange in input
     * @param backstreet Exchange in input
     */
    public ExchangeRateGenerator(Exchange legal, Exchange backstreet) {
        this.legal = legal;
        this.backstreet = backstreet;
    }

    /**
     * Running our thread...
     */
    @Override
    public void run() {
        while (true) {
            try {
                double newRate = rounding(Util.randomDouble(2, 10));
                Thread.sleep(TimeUnit.SECONDS.toMillis(Util.randomInt(5, 10)));
                legal.setExchangeRate(newRate);
                Thread.sleep(TimeUnit.SECONDS.toMillis(1));
                backstreet.setExchangeRate(newRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
