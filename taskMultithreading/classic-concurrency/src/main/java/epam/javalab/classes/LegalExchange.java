package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;
import epam.javalab.interfaces.Tourist;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class represents legal exchange.
 */

public class LegalExchange extends AbstractExchange {

    /**
     * That's  our logger
     */
    private static final Logger logger = LogManager.getLogger(LegalExchange.class);

    /**
     * That's service time for legal exchange
     */
    private static final int LEGAL_SERVICE_TIME = 3000;

    /**
     * That's public constructor
     * @param channelToMafia LowEffortQueue<Double> in input
     * @param exchangeQueue LowEffortQueue<Tourist> in input
     */
    public LegalExchange(LowEffortQueue<Double> channelToMafia, LowEffortQueue<Tourist> exchangeQueue) {
        super(channelToMafia, exchangeQueue);
    }

    /**
     * Sleeping...
     * @throws InterruptedException exception
     */
    @Override
    protected void sleep() throws InterruptedException {
        Thread.sleep(LEGAL_SERVICE_TIME);
    }

    /**
     * Getting logger
     * @return Logger
     */
    @Override
    protected Logger getLogger() {
        return logger;
    }

}
