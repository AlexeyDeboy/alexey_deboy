package epam.javalab.classes;

import java.util.concurrent.ThreadLocalRandom;

/**
 * That's util class
 */

public class Util {

    /**
     * Private constructor.
     */
    private Util() {}

    /**
     * Returns random value
     * @param min double in input
     * @param max double in input
     * @return double value
     */
    public static double randomDouble(double min, double max) {
        return ThreadLocalRandom.current().nextDouble(min, max);
    }

    /**
     * Returns random value
     * @param min int in input
     * @param max int in input
     * @return int value
     */
    public static int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    /**
     * Rounding our value
     * @param value double in input
     * @return double value
     */
    public static double rounding(double value) {
        return Math.round(value * 100.0) / 100.0;
    }
}
