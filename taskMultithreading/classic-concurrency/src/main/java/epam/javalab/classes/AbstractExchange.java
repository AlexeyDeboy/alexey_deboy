package epam.javalab.classes;

import epam.javalab.interfaces.Exchange;
import epam.javalab.interfaces.LowEffortQueue;
import epam.javalab.interfaces.Tourist;
import org.apache.logging.log4j.Logger;

import static epam.javalab.classes.Util.rounding;

/**
 * This class represents an abstract exchange of money.
 * It is better to use BigDecimal for money, but in academic purpose i decided to use double value.
 */

public abstract class AbstractExchange implements Exchange {

    /**
     * This field contains passive money.
     */
    private double passiveMoney;

    /**
     * This field contains active money.
     */
    private double activeMoney;

    /**
     * This field contains exchange rate.
     */
    private double exchangeRate = 2;

    /**
     * This field contains queue - channel to mafia.
     */
    private final LowEffortQueue<Double> channelToMafia;

    /**
     * This field contains queue of exchange.
     */
    private final LowEffortQueue<Tourist> exchangeQueue;

    /**
     * This static field contains threshold of mafia.
     */
    private static final int MAFIA_THRESHOLD = 5;

    /**
     * This static field contains rate of mafia.
     */
    private static final double MAFIA_RATE = 0.1;

    /**
     * This field contains exchange counter.
     */
    private int exchangeCounter;

    /**
     * Public constructor.
     * @param channelToMafia LowEffortQueue<Double> in input
     * @param exchangeQueue LowEffortQueue<Tourist> in input
     */
    public AbstractExchange(LowEffortQueue<Double> channelToMafia, LowEffortQueue<Tourist> exchangeQueue) {
        this.channelToMafia = channelToMafia;
        this.exchangeQueue = exchangeQueue;
        this.exchangeCounter = 0;
        this.passiveMoney = 0;
        this.activeMoney = 0;
    }

    /**
     * Setting exchange rate.
     * @param rate double in input
     */

    @Override
    public void setExchangeRate(double rate) {
        getLogger().info("Changing exchange rate from " + exchangeRate + " to " + rate);
        exchangeRate = rate;
    }


    /**
     * Running our thread...
     */

    @Override
    public void run() {
        channelToMafia.register();
        try {
            while (!exchangeQueue.isEmpty() || !exchangeQueue.isAbandoned()) {
                Tourist tourist = exchangeQueue.dequeue();
                if (tourist != null) {
                    handleExchange(tourist);
                    exchangeCounter++;
                }
                if (exchangeCounter == MAFIA_THRESHOLD) {
                    channelToMafia.enqueue(collectPayment());
                    exchangeCounter = 0;
                }
            }
        } finally {
            channelToMafia.abandon();
        }
        getLogger().info("Total money: " + passiveMoney + activeMoney);
    }

    /**
     * Collecting payment...
     * @return double value
     */

    private double collectPayment() {
        double moneyToMafia = rounding(MAFIA_RATE * activeMoney);
        getLogger().info("Sending " + moneyToMafia + " dollars to mafia.");
        passiveMoney = passiveMoney + (activeMoney - moneyToMafia);
        activeMoney = 0;
        return moneyToMafia;
    }

    /**
     * Handling exchange...
     * @param tourist Tourist in input
     */

    private void handleExchange(Tourist tourist) {
        double moneyToExchange = tourist.takeDollars();
        double moneyExchanged = rounding(exchangeRate * moneyToExchange);
        getLogger().info("Tourist " + tourist.getID() + " exchanges " + moneyToExchange + " dollars for " + moneyExchanged + " bynes.");
        simulateProcess();
        activeMoney = activeMoney + moneyToExchange;
    }

    /**
     * Simulating our process..
     */
    private void simulateProcess() {
        try {
            sleep();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sleeping...
     * @throws InterruptedException exception
     */

    protected abstract void sleep() throws InterruptedException;

    /**
     * Getting our logger...
     * @return Logger
     */
    protected abstract Logger getLogger();
}
