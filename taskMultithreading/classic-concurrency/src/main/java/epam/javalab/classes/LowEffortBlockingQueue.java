package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;

import java.util.LinkedList;
import java.util.Queue;

/**
 * This class represents a blocking queue.
 * @param <T>
 */

public class LowEffortBlockingQueue<T> implements LowEffortQueue<T> {

    /**
     * That's our queue.
     */
    private final Queue<T> queue;

    /**
     * That's our producer counter.
     */
    private int producerCounter = 0;


    /**
     * Public constructor.
     */
    public LowEffortBlockingQueue() {
        this.queue = new LinkedList<>();
    }

    /**
     * This method checks whether an item was successfully added into queue
     * @param item T in input
     * @return boolean value
     */
    public synchronized boolean enqueue(T item) {
        notifyAll();
        return queue.offer(item);
    }

    /**
     * This method get an item from queue.
     * @return T value
     */
    public synchronized T dequeue() {
        T result;
        while ((result = queue.poll()) == null && !isAbandoned()) {
            try {
                wait(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public int getSize() {
        return queue.size();
    }

    @Override
    public synchronized void register() {
        producerCounter++;
    }

    public synchronized void abandon() {
        producerCounter--;
    }

    @Override
    public boolean isAbandoned() {
        return producerCounter == 0;
    }
}
