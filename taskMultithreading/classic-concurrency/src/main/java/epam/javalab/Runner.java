package epam.javalab;

import epam.javalab.classes.*;
import epam.javalab.interfaces.Exchange;
import epam.javalab.interfaces.LowEffortQueue;
import epam.javalab.interfaces.Tourist;

/**
 * This is Runner class
 */
public class Runner {

    /**
     * That's main method
     * @param args in input
     * @throws InterruptedException exception
     */
    public static void main(String[] args) throws InterruptedException {
        LowEffortQueue<Tourist> legalQueue = new LowEffortBoundedBlockingQueue<>(5);
        LowEffortQueue<Tourist> backstreetQueue = new LowEffortBlockingQueue<>();
        LowEffortQueue<Double> channelToMafia = new LowEffortBlockingQueue<>();

        TouristProducer producer = new TouristProducer(legalQueue, backstreetQueue);
        Exchange legalExchange = new LegalExchange(channelToMafia, legalQueue);
        Exchange backstreetExchange = new BackstreetExchange(channelToMafia, backstreetQueue);
        ExchangeRateGenerator generator = new ExchangeRateGenerator(legalExchange, backstreetExchange);
        Mafia mafia = new Mafia(channelToMafia);

        Thread producerThread = new Thread(producer, "Producer");
        Thread mafiaThread = new Thread(mafia, "Mafia");
        Thread legalThread = new Thread(legalExchange, "Legal");
        Thread backThread = new Thread(backstreetExchange, "Illegal");
        Thread generatorThread = new Thread(generator, "Generator");
        generatorThread.setDaemon(true);
        producerThread.start();
        generatorThread.start();
        legalThread.start();
        backThread.start();
        mafiaThread.start();
        producerThread.join();
    }
}