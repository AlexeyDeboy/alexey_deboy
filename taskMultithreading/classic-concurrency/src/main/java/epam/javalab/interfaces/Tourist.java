package epam.javalab.interfaces;

/**
 * This interface represents a tourist
 */
public interface Tourist {

    /**
     * This method returns an id of tourist
     * @return String value
     */
    String getID();

    /**
     * This method returns bucks that tourist takes :)
     * @return double value
     */
    double takeDollars();
}
