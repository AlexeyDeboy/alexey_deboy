package epam.javalab.interfaces;

/**
 * This interface represents low effort queue.
 * @param <T>
 */

public interface LowEffortQueue<T> {

    /**
     * This method add an item to queue and returns true if it was successful.
     * @param item T in input
     * @return boolean value
     */
    boolean enqueue(T item);

    /**
     * This method get an item from queue.
     * @return T value
     */
    T dequeue();

    /**
     * This method increments producer counter by 1.
     */
    void register();

    /**
     * This method decrease producer counter by 1.
     */
    void abandon();

    /**
     * This method checks wether producer counter is zero. If it's zero - return true.
     * @return boolean value
     */
    boolean isAbandoned();

    /**
     * Getting size of our queue.
     * @return int value
     */
    int getSize();

    /**
     * Checks wether our queue is empty. Returns true if it's empty.
     * @return boolean value
     */
    boolean isEmpty();

}
