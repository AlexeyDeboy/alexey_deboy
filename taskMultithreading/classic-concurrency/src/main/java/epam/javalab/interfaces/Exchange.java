package epam.javalab.interfaces;


/**
 * This interface represents exchange of money
 */
public interface Exchange extends Runnable {

    /**
     * This method setting exchange rate
     * @param rate double in input
     */
    void setExchangeRate(double rate);
}
