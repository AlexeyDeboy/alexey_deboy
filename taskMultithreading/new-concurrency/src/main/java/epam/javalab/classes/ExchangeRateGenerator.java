package epam.javalab.classes;

import epam.javalab.interfaces.Exchange;
import java.util.concurrent.TimeUnit;

import static epam.javalab.classes.Util.rounding;

public class ExchangeRateGenerator implements Runnable {
    private final Exchange legal;
    private final Exchange backstreet;

    public ExchangeRateGenerator(Exchange legal, Exchange backstreet) {
        this.legal = legal;
        this.backstreet = backstreet;
    }

    @Override
    public void run() {
        while (true) {
            try {
                double newRate = rounding(Util.randomDouble(2, 10));
                Thread.sleep(TimeUnit.SECONDS.toMillis(Util.randomInt(5, 10)));
                legal.setExchangeRate(newRate);
                Thread.sleep(TimeUnit.SECONDS.toMillis(1));
                backstreet.setExchangeRate(newRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
