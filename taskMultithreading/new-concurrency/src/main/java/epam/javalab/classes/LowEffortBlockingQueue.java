package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;

import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class LowEffortBlockingQueue<T> implements LowEffortQueue<T> {
    private final LinkedTransferQueue<T> queue = new LinkedTransferQueue<>();
    private final AtomicInteger producerCounter = new AtomicInteger();

    public LowEffortBlockingQueue() {}

    public boolean enqueue(T item) {
        return queue.offer(item);
    }

    public T dequeue() {
        return queue.poll();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public int getSize() {
        return queue.size();
    }

    @Override
    public void register() {
        producerCounter.incrementAndGet();
    }

    public void abandon() {
        producerCounter.decrementAndGet();
    }

    @Override
    public boolean isAbandoned() {
        return producerCounter.get() == 0;
    }
}
