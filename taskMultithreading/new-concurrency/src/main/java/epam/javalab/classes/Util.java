package epam.javalab.classes;

import java.util.concurrent.ThreadLocalRandom;

public class Util {

    private Util() { }

    public static double randomDouble(double min, double max) {
        return ThreadLocalRandom.current().nextDouble(min, max);
    }

    public static int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public static double rounding(double value) {
        return Math.round(value * 100.0) / 100.0;
    }
}
