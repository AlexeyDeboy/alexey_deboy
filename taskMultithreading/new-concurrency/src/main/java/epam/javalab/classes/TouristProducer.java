package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;
import epam.javalab.interfaces.Tourist;

public class TouristProducer implements Runnable {
    private final LowEffortQueue<Tourist> legalQueue;
    private final LowEffortQueue<Tourist> backstreetQueue;

    public TouristProducer(LowEffortQueue<Tourist> legalQueue, LowEffortQueue<Tourist> backstreetQueue) {
        this.legalQueue = legalQueue;
        this.backstreetQueue = backstreetQueue;
    }

    @Override
    public void run() {
        legalQueue.register();
        backstreetQueue.register();
        try {
            boolean added = false;
            for (int i = 0; i < 12; i++) {
                try {
                    Thread.sleep(Util.randomInt(1000, 2010));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                final int id = i;
                Tourist tourist = new Tourist() {
                    @Override
                    public String getID() {
                        return id + "";
                    }

                    @Override
                    public double takeDollars() {
                        return 100;
                    }
                };
                if (i % 2 == 0) {
                    added = legalQueue.enqueue(tourist);
                    if (!added) {
                        backstreetQueue.enqueue(tourist);
                    }
                } else {
                    backstreetQueue.enqueue(tourist);
                }
            }
        } finally {
            legalQueue.abandon();
            backstreetQueue.abandon();
        }
    }
}
