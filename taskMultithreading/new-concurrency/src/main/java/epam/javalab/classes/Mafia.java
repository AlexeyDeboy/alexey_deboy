package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Mafia implements Runnable {
    private static final Logger logger = LogManager.getLogger(Mafia.class);
    private final LowEffortQueue<Double> channelToExchangers;
    private double mafiaLedger;

    public Mafia(LowEffortQueue<Double> channelToExchangers) {
        this.channelToExchangers = channelToExchangers;
        this.mafiaLedger = 0;
    }

    @Override
    public void run() {
        while (!channelToExchangers.isAbandoned()) {
            while (!channelToExchangers.isEmpty()) {
                double moreMoney = channelToExchangers.dequeue();
                if (moreMoney != 0) {
                    mafiaLedger = mafiaLedger + moreMoney;
                }
            }
        }
        logger.info("Total mafia ledger: " + mafiaLedger);
    }
}
