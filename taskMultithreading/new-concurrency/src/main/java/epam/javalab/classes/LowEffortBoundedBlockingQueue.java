package epam.javalab.classes;

public class LowEffortBoundedBlockingQueue<T> extends LowEffortBlockingQueue<T> {
    private final int capacity;

    public LowEffortBoundedBlockingQueue(int capacity) {
        super();
        this.capacity = capacity;
    }

    @Override
    public synchronized boolean enqueue(T item) {
        if (getSize() < capacity) {
            return super.enqueue(item);
        }
        return false;
    }
}
