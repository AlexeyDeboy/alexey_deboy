package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;
import epam.javalab.interfaces.Tourist;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static epam.javalab.classes.Util.rounding;

public class BackstreetExchange extends AbstractExchange {
    private static final Logger logger = LogManager.getLogger(BackstreetExchange.class);
    private static final double BACKSTREET_RATE = 0.95;
    public static final int BACKSTREET_SERVICE_TIME = 1000;

    public BackstreetExchange(LowEffortQueue<Double> channelToMafia, LowEffortQueue<Tourist> exchangeQueue) {
        super(channelToMafia, exchangeQueue);
    }

    @Override
    protected void sleep() throws InterruptedException {
        Thread.sleep(BACKSTREET_SERVICE_TIME);
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void setExchangeRate(double rate) {
        double actualRate = rounding(BACKSTREET_RATE * rate);
        super.setExchangeRate(actualRate);
    }
}
