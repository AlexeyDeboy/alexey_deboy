package epam.javalab.classes;

import epam.javalab.interfaces.Exchange;
import epam.javalab.interfaces.LowEffortQueue;
import epam.javalab.interfaces.Tourist;
import org.apache.logging.log4j.Logger;

import static epam.javalab.classes.Util.rounding;

public abstract class AbstractExchange implements Exchange {
    private double passiveMoney;
    private double activeMoney;
    private double exchangeRate = 2;
    private final LowEffortQueue<Double> channelToMafia;
    private final LowEffortQueue<Tourist> exchangeQueue;
    private static final int MAFIA_THRESHOLD = 5;
    private static final double MAFIA_RATE = 0.1;
    private int exchangeCounter;

    public AbstractExchange(LowEffortQueue<Double> channelToMafia, LowEffortQueue<Tourist> exchangeQueue) {
        this.channelToMafia = channelToMafia;
        this.exchangeQueue = exchangeQueue;
        this.exchangeCounter = 0;
        this.passiveMoney = 0;
        this.activeMoney = 0;
    }

    @Override
    public void setExchangeRate(double rate) {
        getLogger().info("Changing exchange rate from " + exchangeRate + " to " + rate);
        exchangeRate = rate;
    }

    @Override
    public void run() {
        channelToMafia.register();
        try {
            while (!exchangeQueue.isAbandoned()) {
                while (!exchangeQueue.isEmpty()) {
                    Tourist tourist = exchangeQueue.dequeue();
                    if (tourist != null) {
                        handleExchange(tourist);
                        exchangeCounter++;
                    }
                }
                if (exchangeCounter == MAFIA_THRESHOLD) {
                    channelToMafia.enqueue(collectPayment());
                    exchangeCounter = 0;
                }
            }
        } finally {
            channelToMafia.abandon();
        }
        getLogger().info("Total money: " + passiveMoney + activeMoney);
    }

    private double collectPayment() {
        double moneyToMafia = rounding(MAFIA_RATE * activeMoney);
        getLogger().info("Sending " + moneyToMafia + " dollars to mafia.");
        passiveMoney = passiveMoney + (activeMoney - moneyToMafia);
        activeMoney = 0;
        return moneyToMafia;
    }

    private void handleExchange(Tourist tourist) {
        double moneyToExchange = tourist.takeDollars();
        double moneyExchanged = rounding(exchangeRate * moneyToExchange);
        getLogger().info("Tourist " + tourist.getID() + " exchanges " + moneyToExchange + " dollars for " + moneyExchanged + " bynes.");
        simulateProcess();
        activeMoney = activeMoney + moneyToExchange;
    }

    private void simulateProcess() {
        try {
            sleep();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected abstract void sleep() throws InterruptedException;

    protected abstract Logger getLogger();
}
