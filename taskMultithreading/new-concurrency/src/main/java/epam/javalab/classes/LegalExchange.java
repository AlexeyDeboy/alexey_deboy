package epam.javalab.classes;

import epam.javalab.interfaces.LowEffortQueue;
import epam.javalab.interfaces.Tourist;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LegalExchange extends AbstractExchange {
    private static final Logger logger = LogManager.getLogger(LegalExchange.class);
    public static final int LEGAL_SERVICE_TIME = 2000;

    public LegalExchange(LowEffortQueue<Double> channelToMafia, LowEffortQueue<Tourist> exchangeQueue) {
        super(channelToMafia, exchangeQueue);
    }

    @Override
    protected void sleep() throws InterruptedException {
        Thread.sleep(LEGAL_SERVICE_TIME);
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
