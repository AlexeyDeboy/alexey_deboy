package epam.javalab.interfaces;

public interface Tourist {
    String getID();
    double takeDollars();
}
