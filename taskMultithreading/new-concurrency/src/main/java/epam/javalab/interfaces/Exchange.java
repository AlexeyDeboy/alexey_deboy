package epam.javalab.interfaces;

public interface Exchange extends Runnable {
    void setExchangeRate(double rate);
}
