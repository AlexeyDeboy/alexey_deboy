package epam.javalab.interfaces;

public interface LowEffortQueue<T> {
    boolean enqueue(T item);
    T dequeue();
    void register();
    void abandon();
    boolean isAbandoned();
    int getSize();
    boolean isEmpty();
}
